
class Node:
	def __init__(self, data):
		self.data = data
		self.next = None

class Stack:

	def __init__ (self):
		self.top = None

	def isEmpty(self):
		return True if self.top is None else False

	def Push(self, data):
		new_node = Node(data)
		new_node.next = self.top
		self.top = new_node
		print ("{} pushed to stack".format(self.top.data))

	def Pop(self):
		if self.isEmpty():
			return print("Stack is empty")
		temp = self.top
		self.top = self.top.next
		print ("{} popped from stack".format(temp.data))
		del temp

def first_call():
	St_ = Stack()
	St_.Push(55)
	St_.Push(0)
	St_.Push(11)
	St_.Pop()
	St_.Pop()

if __name__ == "__main__":
	first_call()