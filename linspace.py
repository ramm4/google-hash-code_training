import numpy as 				np
import matplotlib.pyplot as	plt

N = 8
y = np.zeros(N)

print(y)


import math
import os
import random
import re
import sys

# Complete the cutTheSticks function below.
def cutTheSticks(arr):
    arr_temp = list()
    arr_temp.append(len(arr))
    while True:
        arr = [int(x) for x in arr if x != min(arr)]
        arr_size = len(arr)
        if arr_size > 0:
            arr_temp.append(arr_size)
        else:
            break;
    return arr_temp

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input())

    arr = list(map(int, input().rstrip().split()))

    result = cutTheSticks(arr)

    fptr.write('\n'.join(map(str, result)))
    fptr.write('\n')

    fptr.close()