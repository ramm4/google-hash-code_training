#!/usr/bin/env python3

import sys

def problem_solving(matrix, init_data):
	print(matrix)

def map_initialisation(init_data, file):

	matrix = []
	available_supplements =	{'T' : "Tomato", 'M' : "Mushroom", '\n' : "newline"}

	for idx, i in enumerate(file):
		if idx >= int(init_data[0]):
			print("Incorrect rows amount")
			return None
		for jdx, j in enumerate(i):
			if jdx - 1 >= int(init_data[1]):
				print("Incorrect column amount", jdx)
				return None
			if not j in available_supplements.keys():
				print ("Unexpected symbol {}".format(j))
		if jdx != int(init_data[1]):
			print("Incorrect column1 amount", jdx)
			return None
		matrix.append(i)
	if idx + 1 != int(init_data[0]):
		print("Incorrect rows amount")
		return None
	else:
		return matrix

def parsing_initial_data(file):
	init_data = (file.readline()).split()
	if len(init_data) != 4:
		print("IncØrect initial data")
		return None, 1
	for elem in init_data:
		if int(elem) > 1000:
			print("IncØrect initial data " + elem)
			return None, 1
	return init_data, 0

def first_call():
	if len(sys.argv) != 2:
		print ("Could not open file. Invalid argument")
	else:
		try:
			with open(sys.argv[1], 'r') as file:
				init_data, error = parsing_initial_data(file)
				if error == True:
					pass
				else:
					matrix = map_initialisation(init_data, file);
					if matrix == None:
						pass
					else:
						problem_solving(matrix, init_data)
		except IOError:
			print("No such file")

if __name__ == "__main__":
	first_call()