
import numpy as np
import sys
import time

def read_input():
    all_lines = open(sys.argv[1]).readlines()
    start = time.time()
    amount = all_lines[0]
    c_c_c = 0
    chain = []
    v_list = ([[id_x] + (row.split()) for id_x, row in enumerate(all_lines[1:]) if row[0] == 'V'])
    h_list = ([[id_x] + (row.split()) for id_x, row in enumerate(all_lines[1:]) if row[0] == 'H'])
    for a in v_list:
    	c_c_c = 0
    	for b in v_list:
    		if [a_w in b_w for a_w in a[3:] for b_w in b[3:]]:
    			c_c_c += 1;
    			if c_c_c == (len(a[3:])) if len(a[3:]) < len(b[3:]) else (len(b[3:])):
    				break;
    	if c_c_c > 0:
    		chain.append([a[0], b[0]])
    print(chain)
    end = time.time()
    print(end - start)


if __name__ == "__main__":
    read_input()